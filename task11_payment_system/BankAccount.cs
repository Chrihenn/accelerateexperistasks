﻿
namespace task11_payment_system
{
    class BankAccount
    {
        public double cost { get; set; }
        public double savings { get; set; }
        public double credit { get; set; }

        public BankAccount(double savings, double credit, double cost)
        {
            this.cost = cost;
            this.savings = savings;
            this.credit = credit;
        }

        public BankAccount()
        {

            cost = 0;
        }

        public BankAccount(double savings, double credit)
        {
            this.savings = savings;
            this.credit = credit;
        }

    }
}

﻿using System;
using System.Collections.Generic;

namespace task9_animal
{
    class Program
    {
        public static List<Animal> animals = new List<Animal>();
        static void Main(string[] args)
        {
            animals.Add(new Animal("Tiger", 4, true));
            animals.Add(new Animal("Cow", 4, false));
            animals.Add(new Animal("Monkey", 2, false));
            animals.Add(new Animal("Fish", 0, true));
            animals.Add(new Animal("Cat", 4, true));

            Console.Title = "ANIMALS APP\n";
            Console.WriteLine($"{Console.Title}");
            DisplayOptions();
        }

        public static void DisplayOptions()
        {
            string command = "";

            while (command != "exit")
            {
                Console.WriteLine("Please enter a command. Possible command options: add/feed/list/exit ");
                command = Console.ReadLine().ToLower();
                switch (command)
                {
                    case "add":
                        AddNewAnimal();
                        break;
                    case "feed":
                        FeedAnimal();
                        break;
                    case "list":
                        ListAllAnimals();
                        break;
                    case "exit":
                        Console.WriteLine("Exiting application...");
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("No command. Try again.");
                        break;
                }
            }
        }

        public static void FeedAnimal()
        {
            Console.WriteLine("This function is not available yet");
        }

        public static void ListAllAnimals()
        {
            if (animals.Count == 0)
            {
                Console.WriteLine("No animals registered. Press any key to continue.");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Animals in stock:\n");
            foreach (var animal in animals)
            {
                PrintAnimal(animal);
            }
            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }

        public static void AddNewAnimal()
        {
            Console.Write("Enter what species you are adding: ");
            string type = Console.ReadLine();

            Console.Write("Enter how many legs the animal has: ");
            int legs = Convert.ToInt32(Console.ReadLine());

            Console.Write("Has the animal been fed? [yes/no]: ");
            string input = Console.ReadLine().ToLower();

            if ((type != null) && legs < 0 && input == null)
            {
                animals.Add(new Animal(type));
            }
            else
            {

                if (input == "yes")
                {
                    animals.Add(new Animal(type, legs, true));

                }
                else
                {
                    animals.Add(new Animal(type, legs, false));
                }

                Console.WriteLine($"{type} added to stock.");
            }
        }

        public static void PrintAnimal(Animal animal)
        {
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine("Species: " + animal.species);
            Console.WriteLine("Number of legs: " + animal.numberOflegs);
            Console.WriteLine("Fed: " + animal.fed);
            Console.WriteLine("-------------------------------------------");
        }
    }
}

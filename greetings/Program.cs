﻿/* 
 * TASK 1. Greetings.
 * Write a console application
 * The program must:
 * Display a console message which both greets me •
 * and tells me your name 
 */

using System;

namespace greetings
{
    class Program
    {
        static void Main(string[] args)
        {
            string question;
            Console.Write("Hi Craig! Do you know my name? [y/n] ");
            question = Console.ReadLine();

            if (question == "n")
            {
                Console.WriteLine("You entered: '{0}'. Shame. My name is Christopher T. Hennum.", question);
            }
            else
            {
                Console.WriteLine("You entered: '{0}'. Ok, cool. Bye!", question);
            }
        }
    }
}

﻿using System;

namespace task10_upgrades_namesearch
{
    class Program
    {
        //public static List<Person> People = new List<Person>();

        static void Main(string[] args)
        {
            Console.Title = "CONTACT INFORMATION APP";
            Console.WriteLine("################################");
            Console.WriteLine($"#### {Console.Title} ###");
            Console.WriteLine("################################");
            int command = 0;

            while (command == 0)
            {
                Console.WriteLine("\n Please choose action: \n ");
                Console.WriteLine("1. Quick add contact. (First name and phone number).");
                Console.WriteLine("2. Add new contact. (First name, last name, phone number and email).");
                Console.WriteLine("3. List all contacts.");
                Console.WriteLine("4. Search in contacts.");
                Console.WriteLine("5. Exit application. \n");

                command = Convert.ToInt32(Console.ReadLine());

                switch (command)
                {
                    case 1:
                        // Quick Add
                        Person.QuickAdd();
                        command = 0;
                        break;
                    case 2:
                        // Add Contact
                        Person.AddPerson();
                        command = 0;
                        break;
                    case 3:
                        // List All contacts
                        Person.ListPeople();
                        command = 0;
                        break;
                    case 4:
                        //Search in contacts
                        Person.SearchPeople();
                        command = 0;
                        break;
                    case 5:
                        Console.WriteLine("Exiting application...");
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("No command. Try again.");
                        break;
                }
            }
        }
    }
}

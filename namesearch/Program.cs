﻿/*
 * TASK 5: namesearch.
 * Write a program which stores a sample of 5 contact names
 * First and Last name
 * Then allows the user to search the list by name and determine if the name is in the list
 * Partial matches should work
 * Display all possible matches
 */

using System;
using System.Collections.Generic;

namespace namesearch
{
    class Program
    {
        public static List<Person> People = new List<Person>();

        static void Main(string[] args)
        {
            Console.Title = "#### CONTACT INFORMATION APP #### \n";
            Console.WriteLine($"{Console.Title}");
            string command = "";

            while (command != "exit")
            {
                Console.WriteLine("Please enter a command. Possible command options: add/list/search/exit ");
                command = Console.ReadLine().ToLower();
                switch (command)
                {
                    case "add":
                        AddPerson();
                        break;
                    case "list":
                        ListPeople();
                        break;
                    case "search":
                        SearchPeople();
                        break;
                    case "exit":
                        Console.WriteLine("Exiting application...");
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("No command. Try again.");
                        break;
                }
            }
        }

        public class Person
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }

            
        }

        private static void AddPerson()
        {
            Person person = new Person();

            Console.Write("Enter First Name: ");
            person.FirstName = Console.ReadLine();

            Console.Write("Enter Last Name: ");
            person.LastName = Console.ReadLine();

            People.Add(person);
        }

        private static void PrintPerson(Person person)
        {
            Console.WriteLine("-------------------------------------------");
            Console.WriteLine("First Name: " + person.FirstName);
            Console.WriteLine("Last Name: " + person.LastName);
            Console.WriteLine("-------------------------------------------");
        }

        private static void ListPeople()
        {
            if (People.Count == 0)
            {
                Console.WriteLine("No contacts. Press any key to continue.");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Here are the current people in your address book:\n");
            foreach (var person in People)
            {
                PrintPerson(person);
            }
            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }

        private static void SearchPeople()
        {
            Console.WriteLine("Enter name to search: ");
            string nameToSearch = Console.ReadLine();

            foreach (var person in People)
            {
                if (person.FirstName.Contains(nameToSearch) || person.LastName.Contains(nameToSearch))
                {
                    PrintPerson(person);
                    Console.WriteLine("\nPress any key to continue.");
                    Console.ReadKey();
                    return;
                } 
            }          
        }
    }
}

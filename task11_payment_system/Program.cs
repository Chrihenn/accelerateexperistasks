﻿/*
 * TASK 11. Payment System.
 * Using inheritence, interface. 
 * Can use debit card, credit card, and cash payments.
 * 
 * EXAMPLE/EXPLENATION of cash payment system: 
 * Cash payment change will be returned as an array, like this ex: [1, 0, 0, 1, 0, 0].
 * Each index represents a constant value of currency like this: [100, 50, 20, 10, 5, 1].
 * Example gives 110 money in change.
 * 
 * If you pay less than the cost of the product, the amount that is missing is shown in the same way
 * only with in negative value, like this: [1, 0, 0, -1, 0, 0].
 */

using System;

namespace task11_payment_system
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount ba = new BankAccount(15000.00, 5000.00);
            CreditCard cCard = new CreditCard(ba.credit, ba.savings, ba.cost);
            DebitCard dCard = new DebitCard(ba.credit, ba.savings, ba.cost);


            Transaction t1 = new Transaction("001", "24/12/2019", 103.00);
            Transaction t2 = new Transaction("002", "06/09/2019", 758.00);
            Transaction t3 = new Transaction("003", "04/02/2020", 1000000.00);

            Console.WriteLine("################## BANK TRANSACTIONS #######################\n");
            t1.ShowTransaction();
            t2.ShowTransaction();
            t3.ShowTransaction();
            Console.WriteLine("\n####################################################\n");


            cCard.UseCreditCard(t1.amount, t1.paymentNumber);
            dCard.UseDebitCard(t2.amount, t2.paymentNumber);
            cCard.UseCreditCard(t3.amount, t3.paymentNumber);

            CashWallet cWallet = new CashWallet();
            cWallet.CashPayment();

            Console.ReadKey();
        }
    }
}

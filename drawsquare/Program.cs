﻿/*
 * TASK 3: draw square.
 * Write a program which can be used to print a square of size chosen by the user at runtime.
 * The square can be made of any character that you choose.
 * The file must compile without errors. example: (after compilation) 
 * I use _ to show a space here (Yours should be blank)
 * 
 * #####
 * #   #
 * #   #
 * #   #
 * #####
 */

using System;

namespace drawsquare
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {

                    if (i == 0 || i == 3 || j == 0 || j == 3)
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.Write("\n");
            }
        }
    }
}
﻿/*
 * TASK: 6. BMI Calculator.
 * Write a program that allows the user to input two values:
 * Weight & Height
 * It must calculate BMI and then categorise the result:
 * Underweight: BMI is less than 18.5
 * Normal weight: BMI is 18.5 to 24.9
 * Overweight: BMI is 25 to 29.9
 * Obese: BMI is 30 or more
 */

using System;

namespace bmicalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            double height;
            double weight;
            double bmi;

            Console.WriteLine("Please enter your height (ex. 1.80): ");
            height = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Your height :{0}", height);
            Console.WriteLine("Please enter your weight (ex. 80): ");
            weight = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Your weight :{0}", weight);
            bmi = weight / (height * height);

            Console.WriteLine($"{bmi}");

            switch (bmi)
            {
                case var expression when bmi < 18.5:
                    Console.WriteLine("Underweight.");
                    break;

                case var expression when (bmi > 18.5 && bmi < 24.9):
                    Console.WriteLine("Normal.");
                    break;

                case var expression when (bmi > 25 && bmi < 29.9):
                    Console.WriteLine("Overweight.");
                    break;

                case var expression when (bmi > 30):
                    Console.WriteLine("Obese.");
                    break;

                default:
                    Console.WriteLine("Could not calculate.");
                    break;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task9_animal
{
    class Animal
    {
        
        public string species { get; set; }
        public int numberOflegs { get; set; }
        public bool fed { get; set; }

        public Animal(string species)
        {
            this.species = species;
        }

        public Animal(string species, int numberOflegs, bool fed)
        {
            this.species = species;
            this.numberOflegs = numberOflegs;
            this.fed = fed;
        }
    }
}

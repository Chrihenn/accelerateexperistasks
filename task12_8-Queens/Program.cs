﻿/*
 *  Storm Østberg, Lene Odde og Christopher T. Hennum
 *  TASK 12. 
 */

using System;
using System.Collections.Generic;

class Queens
{

    static int N = 8;
    static int k = 1;
    public int x;
    public int y;

    static void printSolution(int[,] board)
    {
        Console.Write("{0}-\n", k++);
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
                Console.Write(" {0} ", board[i, j]);
            Console.Write("\n");
        }
        Console.Write("\n");
    }

    static bool isSafe(int[,] board, int row, int col)
    {
        int i, j;

       
        /* Check this row on left side */
        for (i = 0; i < col; i++)
            if (board[row, i] == 1)
                return false;

        /* Check this row on left side */
        for (i = 0; i < row; i++)
            if (board[i, col] == 1)
                return false;

        /* Check upper diagonal on left side */
        for (i = row, j = col; i >= 0 && j >= 0; i--, j--)
            if (board[i, j] == 1)
                return false;

        /* Check lower diagonal on left side */
        for (i = row, j = col; j >= 0 && i < N; i++, j--)
            if (board[i, j] == 1)
                return false;


        return true;
    }

    static bool solveNQUtil(int[,] board, int col, int x, int y)
    {
        if (col == N)
        {
            if (board[x, y] == 1)
            {
                printSolution(board);
                return true;
            }
        }

        bool res = false;
        for (int i = 0; i < N; i++)
        {
            if (isSafe(board, i, col))
            {
                board[i, col] = 1;

                res = solveNQUtil(board, col + 1, x,y) || res;

                board[i, col] = 0;
            }
        }
        return res;
    }

    public static void solveNQ(int x, int y)
    {
        int[,] board = new int[N, N];

        if (solveNQUtil(board, 0, x, y) == false)
        {
            Console.Write("Solution does not exist");
            return;
        }
        Console.WriteLine("This coordinate solves 8-queen.");
        return;
    }

    public static void Main()
    {

        Console.WriteLine("Input X-coordinate (0-7): ");
        int x = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Input Y-coordinate (0-7): ");
        int y = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("You entered: {0}, {1}", x, y);
        Console.WriteLine("Displaying possible solutions: ");
        solveNQ(x, y);
    }
}

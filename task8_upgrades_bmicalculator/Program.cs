﻿/*
 * TASK: 8. Upgrade BMI Calculator from earlier task,
 * so that it utilises methods.
 */
using System;

namespace task8_upgrades_bmicalculator
{
    class Program
    {
        public static double height;
        public static double weight;
        public static double bmi;

        static void Main(string[] args)
        {

            PrintInfo();
            InputMeasures();
            CalculateBmi();
            PrintBmi();
            
        }

        public static void PrintInfo()
        {
            Console.WriteLine(" ");
            // Welcome, name, title, instructions etc
        }

        public static void InputMeasures()
        {
            Console.WriteLine("Please enter your height (ex. 1.80): ");
            height = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Your height :{0}", height);
            Console.WriteLine("Please enter your weight (ex. 80): ");
            weight = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Your weight :{0}", weight);
        }

        public static void CalculateBmi()
        {
            bmi = weight / (height * height);

            switch (bmi)
            {
                case var expression when bmi < 18.5:
                    Console.WriteLine("Underweight.");
                    break;

                case var expression when (bmi > 18.5 && bmi < 24.9):
                    Console.WriteLine("Normal.");
                    break;

                case var expression when (bmi > 25 && bmi < 29.9):
                    Console.WriteLine("Overweight.");
                    break;

                case var expression when (bmi > 30):
                    Console.WriteLine("Obese.");
                    break;

                default:
                    Console.WriteLine("Could not calculate.");
                    break;
            }
        }

        public static void PrintBmi()
        {
            Console.WriteLine($"{bmi}");
        }
    }
}

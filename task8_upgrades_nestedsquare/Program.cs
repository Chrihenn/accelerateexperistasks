﻿using System;

namespace task8_upgrades_nestedsquare
{
    class Program
    {
        private static int width;
        private static int height;

        public static int Width { get => width; set => width = value; }
        public static int Height { get => height; set => height = value; }

        static void Main(string[] args)
        {
            InputSize();
            PrintRectangle();
        }

        public static void InputSize()
        {
            Console.WriteLine("Input width: ");
            height = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input width: ");
            width = Convert.ToInt32(Console.ReadLine());
        }

        public static void PrintRectangle()
        {

            for (int i = 0; i < Height; i++)
            {
                for (int j = 0; j < Width; j++)
                {

                    if ((i == 0 || i == Height - 1) || (j == 0 || j == Width - 1))
                    {
                        Console.Write("#");
                    }
                    else if ((i == 2 || i == Height - 3) && (j > 1 && j < Width - 2))
                    {
                        Console.Write("X");
                    }
                    else if ((j == 2 || j == Width - 3) && (i > 1 && i < Height - 2))
                    {
                        Console.Write("%");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.Write("\n");
            }
        }
    }
}


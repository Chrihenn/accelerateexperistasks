﻿/*
 * TASK 4: draw nested square.
 * Write a program which can be used print a rectangle of size chosen by the user at run-time.
 * The rectangle should have an outer edge and a second inner edge.
 * There must be one space between in the inner and outer edge. The rectangle can be made of any character that you choose.
 * (# is probably a good choice)
 * You may choose the orientation yourself. The file must compile without errors. example: (after compilation)
 * I use _ to show a space here (Yours should be blank) 
 */

using System;

namespace nestedsquare
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if ((i == 0 || i == 7 || j == 0 || j == 7) || (i >= 2 &&
                            i < 8 - 2 && j >= 2 && j < 8 - 2) && 
                            (i == 2 || i == 7 - 2 || j == 2 || j == 7 - 2))
                    {     
                        Console.Write("#");
                    }
                    else
                    { 
                        Console.Write(" ");
                    }
                }

                Console.WriteLine();
            }
        }

    }
    
}

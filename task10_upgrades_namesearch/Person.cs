﻿using System;
using System.Collections.Generic;

namespace task10_upgrades_namesearch
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }

        public static List<Person> People = new List<Person>();

        public Person(string firstName, string phoneNumber)
        {
            FirstName = firstName;
            this.phoneNumber = phoneNumber;
        }

        public Person(string firstName, string lastName, string email, string phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            this.email = email;
            this.phoneNumber = phoneNumber;
        }

        public static void QuickAdd()
        {
            // Use constructor with only firstname and phone number 
            // when creating person object.

            Console.Write("Enter first name: ");
            string first = Console.ReadLine().ToLower();
            Console.Write("Enter phone number: ");
            string phone = Console.ReadLine();

            People.Add(new Person(first, phone));
        }

        public static void AddPerson()
        {
            // Use other constructor with firstname, lastname, 
            // phonenumber and email when creating person object.

            Console.Write("Enter first name: ");
            string first = Console.ReadLine().ToLower();

            Console.Write("Enter last name: ");
            string last = Console.ReadLine().ToLower();

            Console.Write("Enter phone number: ");
            string phone = Console.ReadLine();

            Console.Write("Enter email address: ");
            string email = Console.ReadLine();

            People.Add(new Person(first, last, email, phone));
            Console.WriteLine($"SUCCESS: {first} {last} added to contacts.\n");
        }

        public static void PrintPerson(Person person)
        {
            Console.WriteLine("##############################################");
            Console.WriteLine("First Name: " + person.FirstName);
            Console.WriteLine("Last Name: " + person.LastName);
            Console.WriteLine("Phone Number: " + person.phoneNumber);
            Console.WriteLine("Email: " + person.email);
            Console.WriteLine("##############################################");
        }

        public static void ListPeople()
        {
            if (People.Count == 0)
            {
                Console.WriteLine("No contacts. Add contacts through menu. Press any key to continue.");
                Console.ReadKey();
                return;
            }
            Console.WriteLine("Your contacts:\n");
            foreach (var person in People)
            {
                PrintPerson(person);
            }
            Console.WriteLine("\nPress any key to continue.");
            Console.ReadKey();
        }

        public static void SearchPeople()
        {
            Console.WriteLine("Enter name to search: ");
            string nameToSearch = Console.ReadLine();

            foreach (var person in People)
            {
                if (person.FirstName.Contains(nameToSearch) || person.LastName.Contains(nameToSearch))
                {
                    PrintPerson(person);
                    Console.WriteLine("\nPress any key to continue.");
                    Console.ReadKey();
                    return;
                }
            }
        }
    }
}

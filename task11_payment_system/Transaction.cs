﻿using System;

namespace task11_payment_system
{
    class Transaction : ITransaction
    {
        public string paymentNumber;
        public string date;
        public double amount;

        public Transaction()
        {
            paymentNumber = " ";
            date = " ";
            amount = 0.0;
        }

        public Transaction(string paymentNumber, string date, double amount)
        {
            this.paymentNumber = paymentNumber;
            this.date = date;
            this.amount = amount;
        }

        public double GetAmount()
        {
            return amount;
        }

        public void ShowTransaction()
        {
            Console.WriteLine("--------------------------------");
            Console.WriteLine($"Transaction: {paymentNumber}");
            Console.WriteLine($"Date: {date}");
            Console.WriteLine($"Amount: {amount}");
            Console.WriteLine("--------------------------------");
        }
    }
}

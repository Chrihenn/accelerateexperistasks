﻿/*
 * TASK 2. Username.
 * Write a console application
 * The program must:
 * Allow the user to input their name: example “XYZ”
 * Print “Hello XYZ, your name is 3 characters long and starts with a X.”  
 */

using System;

namespace username
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your name: ");
            string userName = Console.ReadLine();

            int nameLength = userName.Replace(" ", "").Length;
            char firstLetter = userName[0];

            string test = char.IsUpper(firstLetter) ? "\n ... and it starts with uppercase!" : "\n ... BTW you should start your name with uppercase!";

            Console.WriteLine("Your name is: '{0}'. Your name is {1} characters long and starts with '{2}' ",
                userName, nameLength, firstLetter);
            Console.WriteLine($"{test}");
        }
    }
}


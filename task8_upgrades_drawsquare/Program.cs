﻿using System;

namespace task8_upgrades_drawsquare
{
    class Program
    {
        private static int width;
        private static int height;

        public static int Width { get => width; set => width = value; }
        public static int Height { get => height; set => height = value; }

        static void Main(string[] args)
        {
            InputSize();
            PrintRectangle();
        }

        public static void InputSize()
        {
            Console.WriteLine("Input width: ");
            height = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Input width: ");
            width = Convert.ToInt32(Console.ReadLine());
        }

        public static void PrintRectangle()
        {

            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {

                    if (i == 0 || i == Width - 1 || j == 0 || j == Height - 1)
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.Write("\n");
            }
        }
    }
}

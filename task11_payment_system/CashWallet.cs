﻿using System;
namespace task11_payment_system
{
    class CashWallet
    {
        private const double _hundred = 100.00;
        private const double _fifty = 50.00;
        private const double _twenty = 20.00;
        private const double _ten = 10.00;
        private const double _five = 05.00;
        private const double _one = 01.00;

        public CashWallet()
        {
        }

        public void CashPayment()
        {
            Console.WriteLine("--------------------------------------");
            Console.WriteLine("Pay with cash?");
            Console.Write("Enter cost of the product (max 100) -> ");
            double costProduct = double.Parse(Console.ReadLine());

            Console.Write("Money given -> ");
            double moneyGiven = double.Parse(Console.ReadLine());


            int[] change = ChangeCalculator(costProduct, moneyGiven);
            Console.WriteLine("Explenation: [100, 50, 20, 10, 5, 1]");
            Console.WriteLine(string.Join(", ", change));
            Console.ReadKey();
        }

        public static int[] ChangeCalculator(double costProduct, double moneyGiven)
        {
                int Calculate(double money, double moneyValue) => (int)(money / moneyValue);
                double change = moneyGiven - costProduct;

                int hundred = Calculate(change, _hundred);
                change -= hundred * _hundred;

                int fifty = Calculate(change, _fifty);
                change -= fifty * _fifty;

                int twenty = Calculate(change, _twenty);
                change -= twenty * _twenty;

                int ten = Calculate(change, _ten);
                change -= ten * _ten;

                int five = Calculate(change, _five);
                change -= five * _five;

                int one = Calculate(change, _one);
                change -= one * _one;

                return new[] { hundred, fifty, twenty, ten, five, one };      
        }
    }
}
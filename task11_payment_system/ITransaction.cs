﻿
namespace task11_payment_system
{
    interface ITransaction
    {
        void ShowTransaction();
        double GetAmount();
    }
}

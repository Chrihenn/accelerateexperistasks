﻿using System;

namespace task11_payment_system
{
    class CreditCard : BankAccount
    {
        public CreditCard(double savings, double credit, double cost) : base(savings, credit, cost)
        {
        }

        public void UseCreditCard(double cost, string nr)
        {
            if (cost > credit)
            {
                Console.WriteLine($"Payment number: {nr}. Transaction failed. Unsufficient funds.");
                return;
            }
            else
            {
                credit -= cost;
                Console.WriteLine($"New balance in credit account: {CheckCredit()}");
            }

        }

        public double CheckCredit()
        {
            return credit;
        }
    }
}

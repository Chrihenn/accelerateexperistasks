﻿using System;

namespace task11_payment_system
{
    class DebitCard: BankAccount
    {
        public DebitCard(double savings, double credit, double cost) : base(savings, credit, cost)
        {
        }

        public void UseDebitCard(double cost, string nr)
        {
            if (cost > savings)
            {
                Console.WriteLine($"Payment number: {nr}. Transaction failed. Unsufficient funds.");
                return;
            }
            else
            {
                savings -= cost;
                Console.WriteLine($"New balance in savings account: {CheckSavings()}");
            }
        }

        public double CheckSavings()
        {
            return savings;
        }
    }
}
